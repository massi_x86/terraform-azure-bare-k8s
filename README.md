# Terraform Azure Bare K8S
This repository will help you create a bare-metal k8s using
Azure's Virtual Machines.

Note, the only purpose of this project is to teach how to
create a CNCF-compatible K8S cluster using normal virtual
machines.  Although it passes the CNCF's test suite, the
created cluster is not HA and for this reason should not be
used in Production.

If you need a production-ready cluster please refer to the
AKS service, which is an excellent choice for a real K8S
cluster.

This project could be used as a starting-point to create your
own cluster or to quickly install a development cluster on an
on-prem cloud.

Although the Terraform scripts rely heavily on Azure's resources,
nothing forbids you in using a different provider. The only
physical resources needed are some virtual machines, and a DNS zone.

The resources which will be created on Azure's side, and for which
you'll be billed, are:
* 3 DS2 Virtual Machines (2vCPU, 4GB RAM, the minimum configuration for the
cluster to be stable)
* 3 Public IPs for the 3 Virtual Machines
* One DNS zone with:
    * 3 A records (1 for each machine)
    * 1 A records (DNS pseudo-loadbalancer)
    * 1 CNAME record (pointing to the pseudo-loadbalancer)
    * 6 CNAME records (1 for each service which will be installed in the cluster)
* 1 VirtualNet for the 3 machines
* 1 Subnet for the 3 machines
* 1 Firewall for the 3 machines, with preconfigured rules for:
    * HTTP
    * HTTPS
    * SSH
    * K8S API
* 2 Storage Disks per machine (one for the OS and one for Gluster)

In my experience, these resources should cost between 90 and 100 euros per month.


## Install the dependencies
First of all, install the needed softwares:
* azure-cli
* terraform
* helm
* kubectl
* ansible

All of them can be easily installed on Mac OS X using brew, 
Windows using Chocolatey and Linux using whatever your package
manager is.

In order to use Ansible on Windows, you'll need Windows 10 PRO and use a Docker
container, it is by far the easiest way to have Ansible on Windows. Going through
the installation process (via pip or Chocolatey) is painful, and I personally would
avoid it.

## Azure subscription ID
If you take a look at the [variables' file](variables.tf) you will
see that there are some things you need to setup before being able
to launch everything. One of these things (and the most important one)
is your Azure's subscription id. Retrieve it using azure-cli:
```shell script
$ az login
$ az account list
```

Your output will be different, we're interested in the ID of the
subscription which will be billed for the resources you'll create,
for example:
```shell script
$ az account list
```
```json
{
    "cloudName": "AzureCloud",
    "id": "THIS IS THE SUBSCRIPTION ID",
    "isDefault": false,
    "name": "My Subscription",
    "state": "Enabled",
    "tenantId": "REDACTED",
    "user": {
      "name": "Massimo.Gengarelli@djingo.onmicrosoft.com",
      "type": "user"
    }
  }
```

Now that you have your subscription ID, put into the `tfvars` file
that you'll use for your cluster, an example of a one-liner for Powershell:
```
$> $subscription = (az account list | ConvertFrom-Json | ? -name -eq "My Subscription").id
$> echo "azure_subscription = `"$subscription`"" >> terraform.tfvars
```

The same for Linux and Mac OS:
```shell script
$ subscription=$(az account list | jq -r '.[] | select(.name=="My Subscription").id')
$ echo "azure_subscription = \"$subscription\"" >> terraform.tfvars
```

## Setup the needed variables

### Domain
To go any further you'll need a registered domain. I personally use GoDaddy and Ionos 
'cause they're quite cheap for european domain names but your mileage may vary.  
When you have chosen a domain, make sure to configure its nameservers to point to the 
Azure ones. Then, store the domain in the `terraform.tfvars` file:
```shell script
$ echo 'domain = "my-kubernetes.mydomain.com"' >> terraform.tfvars
```

### Resource Group Name and Location
All the created resources will be stored in a single Resource group to avoid cluttering
the existing ones. You'll need to choose a name and a region.

To see the list of available regions for your subscription:
```shell script
$ az account list-regions | jq -r '.[].displayName'
```

Or, the same for Powershell:
```shell script
$> az account list-regions | ConvertFrom-Json | % { $_.displayName }
```

For this example, we'll be using the "Germany West Central" region because is the
richest one in Europe for availability of the resources and speed.
```shell script
$ echo 'resource_group_location = "Germany West Central"' >> terraform.tfvars
```

The name of the resource group doesn't really matter, choose one and stick with it.
```shell script
$ echo 'resource_group_name = "Whatever"' >> terraform.tfvars
```


### Your personal information
Finally, you'll need to provide three more variables:
* Your email (will be used to generate the SSL certificates)
* An username (will be the main user for the created VMs)
* A public SSH key (will be used to login to the VMs)

```shell script
$ echo 'email = "my_email@my_domain.com"' >> terraform.tfvars
$ echo 'user = "some_user"' >> terraform.tfvars
$ echo 'public_key = "~/.ssh/id_rsa.pub"' >> terraform.tfvars
```

### The other variables
The scope of the other variables won't be covered in this file, but basically
all the variables defined in [the variables file](variables.tf) can be overridden.

If you change the `machines_size` variable, make sure that the chosen size has at
least 2 CPUs and 4GB of RAM, otherwise the system will be too unstable to be usable.

## Launch Terraform and generate the Ansible files
With everything setup, you can init the Terraform providers, check the plan and
launch everything !
```shell script
$ terraform init
$ terraform plan -out my_plan
```

Before applying the plan, make sure that everything looks good, that all the created
resources match your needs and that no errors get raised. In case of errors, it is most
probably due to some mismatched variables' names.

Now, apply the plan:
```shell script
$ terraform apply my_plan
```

And go grab a coffee, as the creation of all the resources will take between 15 and 30
minutes.

The plan should be executed with no errors and you should see two new files in the
Ansible folder: `ansible/inventory` and `ansible/group_vars/all.yml`.

I won't go into the details of what these files are used for, but check that the
information contained are correct.

## Launch Ansible and create your cluster !
With the Platform in place, we can now start working on the infrastructure. All the
roles are contained in a single Playbook called `install.yml`. Cd into the Ansible
folder and launch the playbook!
```shell script
$ cd ansible
$ export ANSIBLE_HOST_KEY_CHECKING=False
$ ansible-playbook -i inventory install.yml
```

It will take another 10, probably 15 minutes. The roles will install and configure
the following software on the machines:
* kubectl, kubeadm and kubelet
* docker
* Glusterfs (7.3)
* Heketi
* Funnel networking k8s plugin for the Pods

The roles are quite self-explanatory.

I chose not to use the roles already written in the Ansible-Galaxy because I wanted
to learn how to write them by myself. There are probably better solutions on the
Ansible-Galaxy, but we don't really care for now.

## Check your cluster
Now your cluster should be in place, the three nodes should be connected and you
should be able to run your first command to check if the cluster is alive.  You may
have noticed that you have a new file in your ansible folder: `.kubeconfig`, which
corresponds to the Kubernetes configuration of your cluster. Go on and check!
```shell script
$ kubectl --kubeconfig .kubeconfig get nodes
```

You should have the three nodes on `Ready` status (if they're still `NotReady` wait
a couple of minutes, Funnel might take some time to configure everything).

And you should be able to see all your pods running:
```shell script
$ kubectl --kubeconfig .kubeconfig get po -o wide --all-namespaces
```

## Install Gluster, Traefik, Prometheus, Grafana, Jaeger and some test services
It is now time to launch the second playbook: `configure.yml`. This playbook will
install all the needed services into your Kubernetes cluster:
* Traefik version 2.2 (official Helm chart) with the KubernetesCRD provider
and LetsEncrypt TLS configured, Prometheus metrics and Jaeger tracing activated;
* Prometheus Operator, will scrape all the services which we will install;
* Grafana to have a visualisation for your Prometheus scrapes;
* Jaeger to have a collecting Agent for the OpenTracing spans of your services;
* One empty nginx server (to check the PVCs);

On top of that, it will install the excellent Heketi StorageClass for Kubernetes
which will be your main provider for your PVCs. The Heketi driver will communicate
with the Heketi server that we installed during the step before.

```shell script
$ ansible -i inventory configure.yml
```

Once again, it will take some time to install and configure everything.

Once it's done, you can open your browser and point it to the following URLs to
see your cluster in action!
* https://dashboard.yourdomain.com -> Traefik
* https://prometheus.yourdomain.com -> Prometheus
* https://grafana.yourdomain.com -> Grafana
* https://test-pvc.yourdomain.com -> empty nginx



That's all folks!