resource "local_file" "ansible-inventory" {
  filename        = "ansible/inventory"
  file_permission = "0644"
  content = templatefile("ansible_inventory.tpl", {
    master  = var.machines[0]
    workers = slice(var.machines, 1, length(var.machines))
    domain  = var.domain
    user    = var.user
  })
}

resource "local_file" "ansible-vars" {
  filename        = "ansible/group_vars/all.yml"
  file_permission = "0644"
  content = templatefile("ansible_vars.yml.tpl", {
    domain            = var.domain
    user              = var.user
    email             = var.email
    public_ips        = azurerm_network_interface.machine-nic[*].private_ip_address
    loadbalancer_fqdn = azurerm_dns_a_record.loadbalancer.fqdn
  })
}
