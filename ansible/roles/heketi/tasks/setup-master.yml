- name: Check if heketi has already been installed
  stat:
    path: /usr/local/bin/heketi
  register: heketi_bin

- name: Get latest heketi version
  unarchive:
    dest: /root
    remote_src: yes
    src: https://github.com/heketi/heketi/releases/download/v9.0.0/heketi-v9.0.0.linux.amd64.tar.gz
  when: not heketi_bin.stat.exists

- name: Install binary
  shell: |
    cd /root/heketi
    cp heketi /usr/local/bin/heketi
    cp heketi-cli /usr/local/bin/heketi-cli
  when: not heketi_bin.stat.exists

- name: Create heketi user
  user:
    name: heketi
    create_home: no
    generate_ssh_key: no
    password: '!'
    shell: /usr/bin/false

- name: Create heketi paths
  file:
    path: '{{ item }}'
    owner: heketi
    group: heketi
    state: directory
  with_items:
    - /var/lib/heketi
    - /etc/heketi
    - /var/log/heketi

- name: Check if SSH key exists
  stat:
    path: /etc/heketi/id_rsa
  register: heketi_ssh_private_key

- name: Create SSH key for heketi
  shell: ssh-keygen -m PEM -f /etc/heketi/id_rsa -N ''
  when: not heketi_ssh_private_key.stat.exists

- name: Change owner of ssh private key
  file:
    path: /etc/heketi/id_rsa
    owner: heketi
    group: heketi
    state: touch
  when: not heketi_ssh_private_key.stat.exists

- name: Get Heketi public key
  shell: cat /etc/heketi/id_rsa.pub
  register: heketi_ssh_key
  changed_when: no

- name: Set public key for all the hosts
  set_fact:
    heketi_public_key: '{{ heketi_ssh_key.stdout }}'
  delegate_to: '{{ item }}'
  delegate_facts: yes
  with_items: '{{ groups["kube"] }}'

- name: Gather facts for all hosts
  setup:
  with_items: '{{ groups["kube"] }}'

- name: Copy configuration file to /etc/heketi
  template:
    src: heketi-config.json
    dest: /etc/heketi/heketi.json
    owner: heketi
    group: heketi

- name: Copy environment file to /etc/heketi
  template:
    src: heketi-environment.env
    dest: /etc/heketi/heketi.env
    owner: heketi
    group: heketi

- name: Copy Heketi service definition to systemd
  template:
    src: heketi-systemd
    dest: /etc/systemd/system/heketi.service

- name: Copy Heketi topology to /etc/heketi
  template:
    src: heketi-topology.json.j2
    dest: /etc/heketi/topology.json