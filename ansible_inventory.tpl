# Generated automatically by Terraform, DO NOT MODIFY!!!

[kube]
${master} ansible_host=${master}.${domain} ansible_user=${user} k8s_role=master
%{ for worker in workers ~}
${worker} ansible_host=${worker}.${domain} ansible_user=${user} k8s_role=worker
%{ endfor }
