# Generated automatically by Terraform, DO NOT MODIFY !!!

domain: ${domain}

user:
  name: ${user}
  email: ${email}

public_ips: 
%{ for ip in public_ips ~}
  - ${ip}
%{ endfor }

loadbalancer:
  fqdn: ${loadbalancer_fqdn}
