resource "azurerm_dns_zone" "dns-zone" {
  name                = var.domain
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_dns_a_record" "a-records" {
  count               = length(var.machines)
  name                = var.machines[count.index]
  resource_group_name = azurerm_resource_group.rg.name
  zone_name           = azurerm_dns_zone.dns-zone.name
  target_resource_id  = azurerm_public_ip.machine-ip[count.index].id
  ttl                 = 300
}

data "azurerm_public_ip" "allocated_ips" {
  count               = length(var.machines)
  name                = var.machines[count.index]
  resource_group_name = azurerm_resource_group.rg.name
  depends_on = [
    azurerm_linux_virtual_machine.machines,
    azurerm_virtual_machine_data_disk_attachment.machine-disk-attachment
  ]
}

resource "azurerm_dns_a_record" "loadbalancer" {
  name                = "loadbalancer"
  resource_group_name = azurerm_resource_group.rg.name
  zone_name           = azurerm_dns_zone.dns-zone.name
  records             = data.azurerm_public_ip.allocated_ips[*].ip_address
  ttl                 = 300
}

resource "azurerm_dns_cname_record" "cnames" {
  count               = length(var.cname-records)
  name                = var.cname-records[count.index]
  resource_group_name = azurerm_resource_group.rg.name
  zone_name           = azurerm_dns_zone.dns-zone.name
  ttl                 = 300
  record              = azurerm_dns_a_record.loadbalancer.fqdn
}
