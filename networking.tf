resource "azurerm_virtual_network" "vnet" {
  name                = "DefaultNetwork"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "subnet" {
  name                 = "DefaultSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix       = "10.0.42.0/24"
}

resource "azurerm_public_ip" "machine-ip" {
  count               = length(var.machines)
  allocation_method   = "Dynamic"
  resource_group_name = azurerm_resource_group.rg.name
  name                = var.machines[count.index]
  location            = azurerm_resource_group.rg.location
}

resource "azurerm_network_interface" "machine-nic" {
  count               = length(var.machines)
  name                = var.machines[count.index]
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  ip_configuration {
    name                          = "internal"
    primary                       = true
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.subnet.id
    public_ip_address_id          = azurerm_public_ip.machine-ip[count.index].id
  }
}

resource "azurerm_network_security_group" "default-security-group" {
  name                = "DefaultSecurityGroup"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  security_rule {
    name                       = "AllowSSH"
    description                = "Allow traffic to SSH port"
    priority                   = 100
    access                     = "Allow"
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_range     = "22"
    protocol                   = "Tcp"
    direction                  = "Inbound"
  }

  security_rule {
    name                       = "AllowHTTP-HTTPS"
    description                = "Allow traffic to HTTP(s) port"
    priority                   = 200
    access                     = "Allow"
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_ranges    = ["80", "443"]
    protocol                   = "Tcp"
    direction                  = "Inbound"
  }

  security_rule {
    name                       = "AllowK8S"
    description                = "Allow traffic to K8S control port"
    priority                   = 300
    access                     = "Allow"
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_range     = "6443"
    protocol                   = "Tcp"
    direction                  = "Inbound"
  }
}

resource "azurerm_network_interface_security_group_association" "nic-nsg-association" {
  count                     = length(var.machines)
  network_interface_id      = azurerm_network_interface.machine-nic[count.index].id
  network_security_group_id = azurerm_network_security_group.default-security-group.id
}

