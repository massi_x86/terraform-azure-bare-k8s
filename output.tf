output "loadbalancer-fqdn" {
  value = azurerm_dns_a_record.loadbalancer.fqdn
}

output "all-fqdns" {
  value = azurerm_dns_cname_record.cnames[*].fqdn
}

output "ip_address" {
  value = azurerm_public_ip.machine-ip[*].ip_address
  depends_on = [
    azurerm_linux_virtual_machine.machines
  ]
}

output "private_ip_addresses" {
  value = azurerm_network_interface.machine-nic[*].private_ip_address
}

output "fqdns" {
  value = azurerm_dns_a_record.a-records[*].fqdn
}

output "name_servers" {
  value = azurerm_dns_zone.dns-zone.name_servers
}