provider "azurerm" {
  version         = "~> 2.5"
  subscription_id = var.azure_subscription
  features {}
}

provider "local" {
  version = "~> 1.4"
}
