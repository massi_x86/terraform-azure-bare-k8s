variable "azure_subscription" {
  description = "ID of Azure Subscription"
}

variable "resource_group_name" {
  description = "Name of the resource group for the resources"
}

variable "resource_group_location" {
  description = "Location for the resource group"
}

variable "domain" {
  description = "Your own personal domain"
}

variable "user" {
  description = "Admin user of machines"
}

variable "email" {
  description = "Your email (used to create a LetsEncrypt account)"
}

variable "public_key" {
  description = "Path of the public SSH key of the admin user"
}

variable "machines" {
  default = ["black", "red", "white"]
}

variable "machines_size" {
  default = "Standard_B2s"
}

variable "cname-records" {
  description = "CNames to create"
  default     = [
    "dashboard", 
    "test",
    "pvc",
    "prometheus",
    "grafana",
    "jaeger"
  ]
}