resource "azurerm_managed_disk" "machine-disk" {
  count                = length(var.machines)
  resource_group_name  = azurerm_resource_group.rg.name
  location             = azurerm_resource_group.rg.location
  name                 = "${var.machines[count.index]}-gluster"
  create_option        = "Empty"
  disk_size_gb         = 32
  storage_account_type = "Standard_LRS"
}

resource "azurerm_linux_virtual_machine" "machines" {
  count                 = length(var.machines)
  name                  = var.machines[count.index]
  resource_group_name   = azurerm_resource_group.rg.name
  location              = azurerm_resource_group.rg.location
  network_interface_ids = [azurerm_network_interface.machine-nic[count.index].id]
  size                  = var.machines_size
  admin_username        = var.user
  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "19.10-DAILY"
    version   = "latest"
  }
  admin_ssh_key {
    username   = var.user
    public_key = file(var.public_key)
  }
  os_disk {
    caching              = "ReadWrite"
    name                 = "${var.machines[count.index]}-os"
    storage_account_type = "Standard_LRS"
    disk_size_gb         = 50
  }
}

resource "azurerm_virtual_machine_data_disk_attachment" "machine-disk-attachment" {
  count              = length(var.machines)
  caching            = "ReadWrite"
  lun                = "10"
  managed_disk_id    = azurerm_managed_disk.machine-disk[count.index].id
  virtual_machine_id = azurerm_linux_virtual_machine.machines[count.index].id
}

